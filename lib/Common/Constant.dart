import 'package:flutter/material.dart';

import 'HexColor.dart';

String HOME_SCREEN='/HomeScreen';

Color primaryColour = HexColor.fromHex('#3693CE');
Color primaryDarkColour = HexColor.fromHex('#0D669A');
Color primaryYoungColour = HexColor.fromHex('#2073B4');
MaterialAccentColor accentColour = Colors.yellowAccent;
Color? horizontalColour = Colors.blueGrey[300];
Color backgroundColour = Colors.white;

