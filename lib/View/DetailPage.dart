import 'dart:ui';

import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tagging/flutter_tagging.dart';
import 'package:mobx/mobx.dart';
import 'package:movie1/Common/Constant.dart';
import 'package:movie1/Common/MessageToast.dart';
import 'package:movie1/Model/TagsModel.dart';
import 'package:movie1/Model/counter.dart';
import 'package:movie1/Model/movies.dart';

final counter = Counter();

class DetailPage extends StatefulWidget {
  final int id;
  final Movies movies;

  DetailPage({@PathParam('id') required this.id,
    required this.movies});

  @override
  _DetailPageState createState() => new _DetailPageState();
}

class _DetailPageState extends State<DetailPage>
    with SingleTickerProviderStateMixin {
  TextEditingController titleController = TextEditingController();
  TextEditingController directorController = TextEditingController();
  TextEditingController summaryController = TextEditingController();
  late List<TagsModel> _selectedTags;

  @override
  void initState() {
    super.initState();
    _selectedTags = [];
    if (widget.id != 0) {
      titleController.text = widget.movies.moviesData.where((m) => m.id == widget.id).toList()[0].title;
      directorController.text = widget.movies.moviesData.where((m) => m.id == widget.id).toList()[0].director;
      _selectedTags.addAll(widget.movies.moviesData.where((m) => m.id == widget.id).toList()[0].tags);
      summaryController.text = widget.movies.moviesData.where((m) => m.id == widget.id).toList()[0].summary;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  void addMovie(int id, String title, String director, List<TagsModel> tags,
      String summary) {
    print('add Movie: ' + id.toString());
    MovieData movieData = new MovieData(
        id: id, title: title, director: director, tags: tags, summary: summary);
    widget.movies.add(movieData);
    print('add Movie: ' + widget.movies.moviesData.length.toString());
    showToast(context, 'Add Success');
    AutoRouter.of(context).pop();
  }

  void updateMovie(int id, String title, String director, List<TagsModel> tags,
      String summary) {
    print('update Movie: True');
    MovieData movieData = new MovieData(
        id: id, title: title, director: director, tags: tags, summary: summary);
    widget.movies.update(movieData);
    showToast(context, 'Update Success');
    AutoRouter.of(context).pop();
  }

  void deleteMovie(int id) {
    print('delete Movie: True');
    widget.movies.delete(id);
    showToast(context, 'Delete Success');
    AutoRouter.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          shadowColor: Colors.white,
          elevation: 0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    'assets/images/header.png',
                  ),
                  fit: BoxFit.fill,
                ),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25))),
          ),
          backgroundColor: Colors.white,
          title: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 6.0, 10.0, 4.0),
            child: Row(
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.id == 0 ? 'New Movie' : 'Update Movie',
                      style: TextStyle(
                          fontFamily: 'MontserratSemiBold', fontSize: 14),
                    ),
                  ),
                  flex: 1,
                ),
                Expanded(
                  child: widget.id == 0
                      ? GestureDetector(
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Save',
                                style: TextStyle(
                                    fontFamily: 'MontserratSemiBold',
                                    color: Colors.white,
                                    fontSize: 14),
                              ),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  color: Colors.transparent),
                            ),
                          ),
                          onTap: () {
                            counter.increment();
                            int id = counter.id;
                            addMovie(
                                id,
                                titleController.text,
                                directorController.text,
                                _selectedTags,
                                summaryController.text);
                          },
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            GestureDetector(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'Delete',
                                  style: TextStyle(
                                      fontFamily: 'MontserratSemiBold',
                                      fontSize: 14),
                                ),
                              ),
                              onTap: () {
                                deleteMovie(widget.id);
                              },
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            GestureDetector(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  'Update',
                                  style: TextStyle(
                                      fontFamily: 'MontserratSemiBold',
                                      fontSize: 14),
                                ),
                              ),
                              onTap: () {
                                updateMovie(
                                    widget.id,
                                    titleController.text,
                                    directorController.text,
                                    _selectedTags,
                                    summaryController.text,
                                );
                              },
                            )
                          ],
                        ),
                  flex: 1,
                )
              ],
            ),
          ),
        ),
        body: _formDataPribadi());
  }

  Widget _formDataPribadi() {
    return Container(
      margin: const EdgeInsets.fromLTRB(28.0, 20.0, 28.0, 0),
      height: 500,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _showInputText(titleController, 'Title', TextInputType.text, 100),
          _showInputText(
              directorController, 'Director', TextInputType.name, 30),
          _showTags(),
          _showInputText(
              summaryController, 'Summary', TextInputType.multiline, 250),
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
    );
  }

  Widget _showInputText(TextEditingController controller, String hinttext,
      TextInputType type, int maxLength) {
    return Container(
      margin: const EdgeInsets.only(bottom: 12.0),
      height: controller == summaryController ? 110.0 : 70.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            hinttext,
            style: TextStyle(
              color: primaryDarkColour,
              fontSize: 10,
              fontFamily: 'MontserratSemiBold',
            ),
            textAlign: TextAlign.left,
          ),
          SizedBox(
            height: 8,
          ),
          TextFormField(
              textAlign: TextAlign.start,
              controller: controller,
              maxLength: maxLength,
              maxLines: controller == summaryController ? 5 : 1,
              keyboardType: type,
              autofocus: false,
              style: TextStyle(fontSize: 12.0),
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  counterText: '')),
        ],
      ),
    );
  }

  Widget _showTags() {
    return Container(
        margin: const EdgeInsets.only(bottom: 12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Tags',
              style: TextStyle(
                color: primaryDarkColour,
                fontSize: 10,
                fontFamily: 'MontserratSemiBold',
              ),
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: 8,
            ),
            FlutterTagging<TagsModel>(
              initialItems: _selectedTags,
              textFieldConfiguration: TextFieldConfiguration(
                style: TextStyle(fontSize: 12.0),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'Search Tags',
                  labelText: 'Select Tags',
                ),
              ),
              findSuggestions: TagsService.getTags,
              additionCallback: (value) {
                return TagsModel(
                  name: value,
                  position: 0,
                );
              },
              onAdded: (tag) {
                return tag;
              },
              configureSuggestion: (tags) {
                return SuggestionConfiguration(
                  title: Text(tags.name),
                  additionWidget: Chip(
                    avatar: Icon(
                      Icons.add_circle,
                      color: Colors.white,
                    ),
                    label: Text('Add New Tag'),
                    labelStyle: TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w300,
                    ),
                    backgroundColor: Colors.blue,
                  ),
                );
              },
              configureChip: (tag) {
                return ChipConfiguration(
                  label: Text(tag.name),
                  backgroundColor: Colors.blue,
                  labelStyle: TextStyle(color: Colors.white),
                  deleteIconColor: Colors.white,
                );
              },
              onChanged: () {},
            ),
          ],
        ));
  }

  Widget roundedButton(String buttonLabel, Color bgColor, Color textColor) {
    var btn = new Container(
      padding: EdgeInsets.all(5.0),
      alignment: FractionalOffset.center,
      decoration: new BoxDecoration(
        color: bgColor,
        borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: const Color(0xFF696969),
            offset: Offset(1.0, 6.0),
            blurRadius: 0.001,
          ),
        ],
      ),
      child: Text(
        buttonLabel,
        style: new TextStyle(
            color: textColor, fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );
    return btn;
  }
}

class TagsService {
  static Future<List<TagsModel>> getTags(String query) async {
    await Future.delayed(Duration(milliseconds: 500), null);
    return <TagsModel>[
      TagsModel(name: 'Action', position: 1),
      TagsModel(name: 'Comedy', position: 2),
      TagsModel(name: 'Fantasy', position: 3),
      TagsModel(name: 'Horror', position: 4),
      TagsModel(name: 'Sci-Fi', position: 5),
    ]
        .where((lang) => lang.name.toLowerCase().contains(query.toLowerCase()))
        .toList();
  }
}
