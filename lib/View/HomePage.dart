import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie1/Model/movies.dart';
import 'package:movie1/Router/AppRouter.gr.dart';

final movies = Movies();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _titleFont = const TextStyle(
      fontSize: 14.0, fontFamily: 'MontserratBold', color: Colors.blue);
  final _directorFont =
      const TextStyle(fontSize: 12.0, fontFamily: 'MontserratSemiBold');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.white,
        elevation: 0,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'assets/images/header.png',
                ),
                fit: BoxFit.fill,
              ),
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(25),
                  bottomRight: Radius.circular(25))),
        ),
        backgroundColor: Colors.white,
        title: Padding(
          padding: const EdgeInsets.fromLTRB(40.0, 6.0, 10.0, 4.0),
          child: Row(
            children: [
              Expanded(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Movie',
                    style: TextStyle(
                        fontFamily: 'MontserratSemiBold', fontSize: 14),
                  ),
                ),
                flex: 1,
              ),
              Expanded(
                child: GestureDetector(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Image.asset(
                      'assets/images/buatbaru.png',
                      height: 30,
                      width: 30,
                    ),
                  ),
                  onTap: () {
                    context.router.push(DetailRoute(movies: movies, id: 0));
                  },
                ),
                flex: 1,
              )
            ],
          ),
        ),
      ),
      body: _buildSuggestions(),
    );
  }

  Widget _buildSuggestions() {
    if (movies.moviesData.length > 0) {
      movies.moviesData.sort((a, b) => a.id.compareTo(b.id));
      return ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemCount: movies.moviesData.length,
          itemBuilder: (context, i) {
            return _buildRow(movies.moviesData[i], i);
          });
    } else {
      return Center(
        child: Text('Data Kosong'),
      );
    }
  }

  Widget _buildRow(MovieData movieData, int index) {
    return GestureDetector(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Title',
                      style: _titleFont,
                    ),
                    flex: 35,
                  ),
                  Expanded(
                    child: Text(
                      ':',
                      style: _titleFont,
                    ),
                    flex: 5,
                  ),
                  Expanded(
                    child: Text(
                      movieData.title,
                      style: _titleFont,
                    ),
                    flex: 35,
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Director',
                      style: _directorFont,
                    ),
                    flex: 35,
                  ),
                  Expanded(
                    child: Text(
                      ':',
                      style: _directorFont,
                    ),
                    flex: 5,
                  ),
                  Expanded(
                    child: Text(
                      movieData.director,
                      style: _directorFont,
                    ),
                    flex: 35,
                  )
                ],
              )
            ],
          ),
        ),
      ),
      onTap: () {
        context.router.push(DetailRoute(movies: movies, id: movieData.id));
      },
    );
  }
}
