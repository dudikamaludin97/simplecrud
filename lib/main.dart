import 'package:flutter/material.dart';
import 'package:movie1/Router/AppRouter.gr.dart';

import 'Model/counter.dart';

final counter = Counter();

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
      title: 'Movie',
      debugShowCheckedModeBanner: false,
    );
  }
}
