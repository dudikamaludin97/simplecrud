import 'package:mobx/mobx.dart';
import 'package:movie1/Model/TagsModel.dart';

part 'movies.g.dart';

class Movies = _Movies with _$Movies;

abstract class _Movies with Store {
  @observable
  List<MovieData> moviesData = [];

  @action
  void add(MovieData movieData) {
    moviesData.add(movieData);
  }

  @action
  void update(MovieData movieData) {
    moviesData.removeAt(moviesData.indexWhere((m) => m.id == movieData.id));
    moviesData.add(movieData);
  }

  @action
  void delete(int id) {
    moviesData.removeAt(moviesData.indexWhere((m) => m.id == id));
  }
}

class MovieData {
  @observable
  int id;

  @observable
  String title;

  @observable
  String director;

  @observable
  String summary;

  @observable
  List<TagsModel> tags;

  MovieData({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.tags,
  });
}
