// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movies.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Movies on _Movies, Store {
  final _$moviesDataAtom = Atom(name: '_Movies.moviesData');

  @override
  List<MovieData> get moviesData {
    _$moviesDataAtom.reportRead();
    return super.moviesData;
  }

  @override
  set moviesData(List<MovieData> value) {
    _$moviesDataAtom.reportWrite(value, super.moviesData, () {
      super.moviesData = value;
    });
  }

  final _$_MoviesActionController = ActionController(name: '_Movies');

  @override
  void add(MovieData movieData) {
    final _$actionInfo =
        _$_MoviesActionController.startAction(name: '_Movies.add');
    try {
      return super.add(movieData);
    } finally {
      _$_MoviesActionController.endAction(_$actionInfo);
    }
  }

  @override
  void update(MovieData movieData) {
    final _$actionInfo =
        _$_MoviesActionController.startAction(name: '_Movies.update');
    try {
      return super.update(movieData);
    } finally {
      _$_MoviesActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete(int id) {
    final _$actionInfo =
        _$_MoviesActionController.startAction(name: '_Movies.delete');
    try {
      return super.delete(id);
    } finally {
      _$_MoviesActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
moviesData: ${moviesData}
    ''';
  }
}
