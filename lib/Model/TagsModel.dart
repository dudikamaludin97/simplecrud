import 'package:flutter_tagging/flutter_tagging.dart';

class TagsModel extends Taggable {
  final String name;
  final int position;

  TagsModel({
    required this.name,
    required this.position
  });

  @override
  List<Object> get props => [name];

  String toJson() => '''  {
    "name": $name,\n
    "position": $position\n
  }''';

  @override
  String toString() {
    return name;
  }

}