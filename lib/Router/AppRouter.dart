import 'package:auto_route/annotations.dart';
import 'package:movie1/View/DetailPage.dart';
import 'package:movie1/View/HomePage.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      page: HomePage,
      initial: true,
      path: "/home"
    ),
    AutoRoute(
      page: DetailPage,
      path: "/detail/:id"
    ),
  ],
)
class $AppRouter {}
